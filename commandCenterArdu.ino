#include <EEPROM.h>

#define HEADER_RX  '#'
#define HEADER_TX  '*'
#define SET_DEV_ID 'A'
#define GET_DEV_ID 'B'
#define SET_CONF   'C'
#define GET_CONF   'D'

#define MESSAGE_BYTES 64

int eepDevAddr = 0;
static int devAddr = 0;

char payload[MESSAGE_BYTES];

int procesMsg(void);

void setup()
{

	Serial.begin(9600);

	devAddr = EEPROM.read(eepDevAddr);
	Serial.println(devAddr);
}

void loop()
{
	int lf = 10;

	if (Serial.available() > 0) 
	{
		Serial.readBytesUntil(lf, payload, 64);
		procesMsg();
		
		/*
		for (int i = 0; i < MESSAGE_BYTES; i++)
		{
			Serial.print(payload[i]);
		}
		Serial.println();
		*/
	}
}


int procesMsg(void)
{
	char head = payload[0];
	int addr = atoi(payload + 1);
	char tag = payload[4];

	if (GET_DEV_ID == tag)
	{
		Serial.print("GET_DEV_ID ");
		devAddr = EEPROM.read(eepDevAddr);
		Serial.println(devAddr);
		return 0;
	}

	if (devAddr == addr || 255 == devAddr)
	{
		
		
		if (SET_DEV_ID == tag)
		{
			int id = atoi(payload + 5);
			EEPROM.write(eepDevAddr, id);
			Serial.print("SET_DEV_ID ");
			Serial.println(id);
		}
	}
	else
	{
		Serial.print("Warning: wrong dev addr: ");
		Serial.println(devAddr);
	}
}